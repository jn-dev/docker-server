<?php

namespace App\TemplateService;

use App\Service\ContextService;

/**
 * Class AssetTemplateService
 * @package App\TemplateService
 */
class AssetTemplateService extends AbstractTemplateService
{
    /**
     * @var ContextService
     */
    private $contextService;

    /**
     * AssetTemplateService constructor.
     * @param ContextService $contextService
     */
    public function __construct(ContextService $contextService)
    {
        $this->contextService = $contextService;
    }


    /**
     * Return script for template
     *
     * @param $file
     * @return string
     */
    public function getScript($file)
    {
        return '<script type="text/javascript" src="' . $file . '?v=' . $this->getVersion($file) . '"></script>';
    }

    /**
     * Return style for tamplate
     *
     * @param $file
     * @return string
     */
    public function getStyle($file)
    {
        return '<link rel="stylesheet" type="text/css" href="' . $file . '?v=' . $this->getVersion($file) . '">';
    }

    /**
     * Return file time created of file
     *
     * @param $file
     * @return string
     * @throws \Exception
     */
    private function getVersion($file)
    {
        $filePath = $this->contextService->getParameter('path_public') . $file;

        if(is_file($filePath)) {
            return (string) filemtime($filePath);
        }

        throw new \Exception("Asset file '$file' not found in web path '$filePath'");
    }
}
