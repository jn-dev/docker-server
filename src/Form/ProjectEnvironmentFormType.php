<?php

namespace App\Form;

use App\Entity\CategoryEntity;
use App\Entity\Enum\ProjectEnvironmentTypeEnumType;
use App\Entity\ProductEntity;
use App\Entity\ProjectEntity;
use App\Entity\ProjectEnvironmentEntity;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class ProjectEnvironmentFormType
 * @package App\Form
 */
class ProjectEnvironmentFormType extends AbstractType
{
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            array(
                'data_class' => ProjectEnvironmentEntity::class,
            )
        );
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        /** @var ProjectEnvironmentEntity $env */
        $env = $builder->getData();
        $attrName = [];

        if (in_array(
            $env->getType(),
            [ProjectEnvironmentTypeEnumType::TYPE_PRODUCTION, ProjectEnvironmentTypeEnumType::TYPE_STAGING]
        )) {
            $attrName = ['readonly' => 'readonly'];
        }

        $builder
            ->add('name', null, ['label' => '_name', 'attr' => $attrName])
            ->add('project', TextType::class, ['label' => '_project', 'attr' => ['readonly' => 'readonly']])
            ->add(
                'vars_project',
                TextareaType::class,
                [
                    'mapped' => false,
                    'label' => '_default_vars',
                    'attr' => [
                        'rows' => 10,
                        'readonly' => 'readonly',
                    ],
                    'data' => $env->getProject()->getVars()
                ]
            )
            ->add(
                'vars',
                null,
                [
                    'label' => '_vars',
                    'attr' => ['rows' => 10],
                ]
            )

            ->add(
                'deploymentScenario',
                null,
                [
                    'label' => '_deploymentScenario',
                    'attr' => ['rows' => 10],
                ]
            )
            ->add('save', SubmitType::class, ['label' => '_save']);
    }
}