<?php

namespace App\Form;

use App\Entity\CategoryEntity;
use App\Entity\ProductEntity;
use App\Entity\ProjectEntity;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class ProjectFormType
 * @package App\Form
 */
class ProjectFormType extends AbstractType
{
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            array(
                'data_class' => ProjectEntity::class,
            )
        );
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', null, ['label' => '_name'])
            ->add('identificator', null, ['label' => '_identificator'])
            ->add(
                'dockerComposeTemplate',
                null,
                [
                    'label' => '_docker-compose-template',
                    'attr' => ['rows' => 20],
                ]
            )
            ->add(
                'vars',
                null,
                [
                    'label' => '_vars',
                    'attr' => ['rows' => 10],
                ]
            )
            ->add('save', SubmitType::class, ['label' => '_save']);
    }
}