<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\Email;

class LoginForm extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(
                'email',
                EmailType::class,
                [
                    'label' => 'form.email',
                    'attr' => [
                        'autofocus' => true,
                    ],
                    'constraints' => new Email(),
                ]
            )
            ->add(
                'password',
                PasswordType::class,
                [
                    'label' => 'form.password',
                ]
            )
            ->add(
                'save',
                SubmitType::class,
                [
                    'label' => 'cms.auth.login.action',
                ]
            );
    }
}