# FlashMessage


### Instalace

Musí se povolit session v configu `config/framework.yaml`


Pro použití je třeba také vložit assety

```
  "src/AddOn/FlashMessage/asset/script.js",
  "src/AddOn/FlashMessage/asset/style.less",
```

a do twigu zaregistrovat namespace:

config/packages/twig.yaml

```
twig:
  paths:
    '%kernel.project_dir%/src/AddOn/FlashMessage/template': FlashMessageTemplate
```


Poté do šabblony na správné místo vložit následující snippet, který se postará o vykreslení flashMessage
```
{%  include ('@FlashMessageTemplate/template.html.twig') only %}
```



### Použití v controlleru / service

Pred dependenci injection si vložím do controlleru / service FlashMessageService a pak prostřednictvím následujícího volání vytvořím zprávu

```
$flashMessageService->addErrorMessage('something.is.wrong');
```
Pozn.: Zprávy jsuo v service automaticky překládány, stačí kdyz jšou přeložené v souboru `translation/messages.cs.yaml`
