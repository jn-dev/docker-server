# Datatable

## Instalace

#### Závislosti na ostatních AddOn
@Snippet

#### Uprava konfiguračních souborů:

/trans/messages.cs

```
# DATATABLE ADDON
datatable:
  search: 'Vyhledej'
  numberOfRows: 'Počet záznamů na stránku'
  emptyResult: 'Nebyl nalezen žádný záznam'
  paginator:
    prev: 'předchozí'
    next: 'další'
```


config/packages/twig.yaml

```
twig:
  paths:
    '%kernel.project_dir%/src/AddOn/Datatable/template': DataTableTemplate
```


config/services.yaml
```
services:
  App\AddOn\Snippet\Service\SnippetService:
    arguments:
      - {
          datatable: 'App\AddOn\Datatable\Controller\SnippetDatatableController::template'
        }
```

### Nakopírování assetů do styles.css / scripts.js
```
  "src/AddOn/Datatable/asset/script.js",
  "src/AddOn/Datatable/asset/style.css",
```
## Použití

V controlleru si vytvořím dataSet včetně šablony a pošlu ho na vykreslení.
```
class CategoryController
{
 
  public function default(DatatableService $datatableService)
  {
    $data = [
	    'repository_name' => Category::class,
	    'repository_method' => 'queryBuilderAll',
	    'repository_method_params' => [],
	    'searchColumns' => ['c.id', 'c.label'],
	    'columns' => [
            'id' => [
                'label' => 'id',
                'order' => true,
                'render' => '{{ row.id }}'
            ],
            'label' => [
                'label' => 'label',
                'render' => '{{ row.label }}'
            ],
            'action' => [
                'label' => 'action',
                'render' => "
                    {% include cms_atom('btn') with {
                        href: path(\"cms_error_delete\", {fileName: row.id}),
                        classes: ['btn-success'],
                        label: 'action'|trans,
                    } %}
                "
            ],
        ],
    ];

    return [
        'heading'=> 'category',
        'datatable' => $datatableService->datatable($data),
    ];
  }
     
...
 
}
```
