<?php

namespace App\Repository;

use App\Entity\ProjectEnvironmentEntity;

class ProjectEnvironmentRepository extends AbstractRepository
{
    public static $entityClass = ProjectEnvironmentEntity::class;
    public static $rootQueryBuilderAlias = 'pe';

}
