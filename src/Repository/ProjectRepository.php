<?php

namespace App\Repository;

use App\Entity\ProjectEntity;

class ProjectRepository extends AbstractRepository
{
    public static $entityClass = ProjectEntity::class;
    public static $rootQueryBuilderAlias = 'p';

}
