<?php

namespace App\Controller\Cms;

use App\Entity\ProjectEnvironmentEntity;
use App\Repository\ProjectEnvironmentRepository;
use App\Service\DockerComposeService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Class DeploymentController
 * @package App\Controller\Cms
 */
class DeploymentController
{
    /**
     * @param Request                      $request
     * @param ProjectEnvironmentRepository $environmentRepository
     * @param DockerComposeService         $dockerComposeService
     *
     * @return array
     *
     * @Route("/cms/deployment/deploy/{envId}", name="cms_deployment_deploy", requirements={"envId" = "\d+"}, defaults={
     *     "_roles" = {
     *     App\Service\AuthService::ROLE_ADMIN,
     *     App\Service\AuthService::ROLE_SUPERADMIN,
     * }})
     * @Template("@CmsTemplate/command-result/template.html.twig")
     */
    public function deploy(
        Request $request,
        ProjectEnvironmentRepository $environmentRepository,
        DockerComposeService $dockerComposeService
    ) {
        $env = $environmentRepository->findOneById($request->get('envId', 0));

        if (!$env instanceof ProjectEnvironmentEntity) {
            throw new NotFoundHttpException();
        }

        return [
            'result' => $dockerComposeService->deploy($env),
        ];
    }


}