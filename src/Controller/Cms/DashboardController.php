<?php

namespace App\Controller\Cms;

use App\Repository\ProjectRepository;
use App\Service\ShellService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

/**
 * Class DefaultController
 * @package App\Controller\Cms
 */
class DashboardController
{
    /**
     * @param ProjectRepository $projectRepository
     * @param ShellService      $shellService
     *
     * @return array
     *
     * @Route("/cms/dashboard", name="cms_dashboard_default", defaults={
     *     "_roles" = {
     *     App\Service\AuthService::ROLE_ADMIN,
     *     App\Service\AuthService::ROLE_SUPERADMIN,
     * }})
     * @Template("@CmsTemplate/dashboard/template.html.twig")
     */
    public function default(ProjectRepository $projectRepository, ShellService $shellService)
    {
        //osxfs                   232.6G    217.6G     14.7G  94% /volumes

        $freeSpaceCmd = $shellService->execute('df -Ph | grep /volumes');

        $freeSpace = null;
        if($freeSpaceCmd['exit'] == 0) {
            $info = explode(' ', preg_replace('!\s+!', ' ', $freeSpaceCmd['output'][0]));

            $freeSpace = [
                'total' => $info[1],
                'used' => $info[2],
                'free' => $info[3],
                'used_percentage' => intval($info[4]),
            ];
        }

        return [
            'projects' => $projectRepository->findAll(),
            'hdd' => $freeSpace,
        ];
    }
}