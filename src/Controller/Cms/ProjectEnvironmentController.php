<?php

namespace App\Controller\Cms;

use App\AddOn\FlashMessage\Service\FlashMessageService;
use App\Entity\Enum\ProjectEnvironmentTypeEnumType;
use App\Entity\ProjectEntity;
use App\Entity\ProjectEnvironmentEntity;
use App\Form\ProjectEnvironmentFormType;
use App\Repository\ProjectEnvironmentRepository;
use App\Repository\ProjectRepository;
use App\Service\ProjectService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\RouterInterface;

/**
 * Class ProjectEnvironmentController
 * @package App\Controller\Cms
 */
class ProjectEnvironmentController
{
    /**
     * Create / edit project
     *
     * @param Request                      $request
     * @param ProjectRepository            $projectRepository
     * @param ProjectEnvironmentRepository $environmentRepository
     * @param FormFactoryInterface         $factory
     * @param RouterInterface              $router
     * @param FlashMessageService          $flashMessageService
     * @param ProjectService               $projectService
     *
     * @return array|RedirectResponse
     *
     * @Route("/cms/project/{projectId}/environment/new", name="cms_project_env_new", defaults={
     *     "_roles" = {
     *     App\Service\AuthService::ROLE_ADMIN,
     *     App\Service\AuthService::ROLE_SUPERADMIN,
     * }})
     *
     * @Route(
     *     "/cms/project/{projectId}/environment/edit/{id}",
     *     name="cms_project_env_edit",
     *     requirements={"projectId"="\d+","id"="\d+"},
     *     defaults={
     *      "_roles" = {
     *          App\Service\AuthService::ROLE_ADMIN,
     *          App\Service\AuthService::ROLE_SUPERADMIN,
     * }})
     *
     * @Template("@CmsTemplate/project-environment-form/template.html.twig")
     */
    public function edit(
        Request $request,
        ProjectRepository $projectRepository,
        ProjectEnvironmentRepository $environmentRepository,
        FormFactoryInterface $factory,
        RouterInterface $router,
        FlashMessageService $flashMessageService,
        ProjectService $projectService
    ) {
        $projectId = $request->get('projectId');
        $project = $projectRepository->findOneById($projectId);
        if (!$project instanceof ProjectEntity) {
            throw new NotFoundHttpException();
        }

        $id = $request->get('id');

        $entity = (new ProjectEnvironmentEntity())
            ->setProject($project)
            ->setType(ProjectEnvironmentTypeEnumType::TYPE_TESTING);

        if ($id) {
            $findEntity = $environmentRepository->findOneById($id);
            if ($findEntity instanceof ProjectEnvironmentEntity) {
                $entity = $findEntity;
            } else {
                throw new NotFoundHttpException();
            }
        }

        $form = $factory->create(ProjectEnvironmentFormType::class, $entity);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            //z informacniho hlediska zobrazuju ve formulari projekt, ale zde ho chci mit opet nasetovany spravne
            $entity->setProject($project);

            if ($entity->getId()) {
                $environmentRepository->update($entity);
                $flashMessageService->createSuccessMessage('projectEnvironment.was.updated');
            } else {


                $environmentRepository->insert($entity);

                $flashMessageService->createSuccessMessage('projectEnvironment.was.created');
            }

            $url = $router->generate('cms_project_default');

            return new RedirectResponse($url);
        }


        return [
            'heading' => $entity->getId() ? 'edit.projectEnvironment' : 'add.new.projectEnvironment',
            'form' => $form->createView(),
            'dockerCompose' => $projectService->createDockerCompose($entity),
        ];
    }

    /**
     * Create / edit project
     *
     * @param Request                      $request
     * @param ProjectRepository            $projectRepository
     * @param ProjectEnvironmentRepository $environmentRepository
     * @param RouterInterface              $router
     * @param FlashMessageService          $flashMessageService
     *
     * @return array|RedirectResponse
     *
     * @Route(
     *     "/cms/project/{projectId}/environment/remove/{id}",
     *     name="cms_project_env_remove",
     *     requirements={"projectId"="\d+","id"="\d+"},
     *     defaults={
     *      "_roles" = {
     *          App\Service\AuthService::ROLE_ADMIN,
     *          App\Service\AuthService::ROLE_SUPERADMIN,
     * }})
     */
    public function remove(
        Request $request,
        ProjectRepository $projectRepository,
        ProjectEnvironmentRepository $environmentRepository,
        RouterInterface $router,
        FlashMessageService $flashMessageService
    ) {
        $id = $request->get('id');
        $environment = $environmentRepository->findOneById($id);
        if (!$environment instanceof ProjectEnvironmentEntity) {
            throw new NotFoundHttpException();
        }

        if ($environment->getType() == ProjectEnvironmentTypeEnumType::TYPE_TESTING) {
            $environmentRepository->remove($environment);
            $flashMessageService->createSuccessMessage('projectEnvironment.was.deleted');
        } else {
            $flashMessageService->createSuccessMessage('projectEnvironment.cannot.be.deleted');
        }


        $url = $router->generate('cms_project_default');

        return new RedirectResponse($url);
    }

}