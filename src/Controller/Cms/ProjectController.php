<?php

namespace App\Controller\Cms;

use App\AddOn\Datatable\Service\DatatableService;
use App\AddOn\FlashMessage\Service\FlashMessageService;
use App\Entity\Enum\ProjectEnvironmentTypeEnumType;
use App\Entity\ProjectEntity;
use App\Entity\ProjectEnvironmentEntity;
use App\Form\ProjectFormType;
use App\Repository\ProjectEnvironmentRepository;
use App\Repository\ProjectRepository;
use App\Service\ShellService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\RouterInterface;

/**
 * Class ProjectController
 * @package App\Controller\Cms
 */
class ProjectController
{
    /**
     * @param DatatableService $datatableService
     * @param RouterInterface  $router
     *
     * @return array
     *
     * @Route("/cms/project", name="cms_project_default", defaults={
     *     "_roles" = {
     *     App\Service\AuthService::ROLE_ADMIN,
     *     App\Service\AuthService::ROLE_SUPERADMIN,
     * }})
     * @Template("@CmsTemplate/datatable/template.html.twig")
     */
    public function default(DatatableService $datatableService, RouterInterface $router)
    {
        $alias = ProjectRepository::$rootQueryBuilderAlias;
        $data = [
            'repository_name' => ProjectRepository::getName(),
            'repository_method' => 'queryBuilderAll',
            'repository_method_params' => [],
            'searchColumns' => ["$alias.id", "$alias.name"],
            'newLink' => $router->generate('cms_project_new'),
            'newLabel' => 'add.new.project',
            'columns' => [
                'id' => [
                    'label' => '_id',
                    'order' => true,
                    'render' => '{{ row.id }}',
                ],
                'email' => [
                    'label' => '_name',
                    'render' => '{{ row.name }}',
                ],
                'env' => [
                    'label' => '_environment',
                    'render' => " 
                        {% for env in row.projectEnvironments  %}
                            {% if env.type in ['staging', 'production'] %}
                                <a href='{{ path('cms_project_env_edit', { projectId: row.id, id: env.id }) }}' class='btn  btn-default  btn-primary btn-xs'>{{ env.type }}</a>
                            {% else %}
                                 <div class='btn-group' role='group'>
                                    <a href='{{ path('cms_project_env_edit', { projectId: row.id, id: env.id }) }}' class='btn  btn-default  btn-primary btn-xs'>{{ env.name }}</a>
                                    <a href='{{ path('cms_project_env_remove', { projectId: row.id, id: env.id }) }}' class='btn  btn-default  btn-danger btn-xs' onclick='return confirm(\"Sure?\")' >X</a>
                                </div>
                            {% endif %}
                        {% endfor %}
                        <a href='{{ path('cms_project_env_new', { projectId: row.id }) }}' class='btn  btn-default  btn-success btn-xs'>+ add</a>
                     ",
                ],
                'action' => [
                    'label' => '_action',
                    'render' => "
                        {% include cms_atom('btn') with {
                            href: path('cms_project_edit', {id: row.id}),
                            classes: ['btn-primary'],
                            label: '_edit'|trans,
                        } %}
                    ",
                ],
            ],
        ];

        return [
            'heading' => '_projects',
            'datatable' => $datatableService->datatable($data),
        ];
    }

    /**
     * Create / edit project
     *
     * @param Request                      $request
     * @param ProjectRepository            $repository
     * @param ProjectEnvironmentRepository $repositoryEnv
     * @param FormFactoryInterface         $factory
     * @param RouterInterface              $router
     * @param FlashMessageService          $flashMessageService
     * @param ShellService                 $shellService
     *
     * @return array|RedirectResponse
     *
     * @Route("/cms/project/new", name="cms_project_new", defaults={
     *     "_roles" = {
     *     App\Service\AuthService::ROLE_ADMIN,
     *     App\Service\AuthService::ROLE_SUPERADMIN,
     * }})
     *
     * @Route("/cms/project/{id}/edit", name="cms_project_edit", requirements={"id" = "\d+"}, defaults={
     *     "_roles" = {
     *     App\Service\AuthService::ROLE_ADMIN,
     *     App\Service\AuthService::ROLE_SUPERADMIN,
     * }})
     *
     * @Template("@CmsTemplate/form/template.html.twig")
     */
    public function edit(
        Request $request,
        ProjectRepository $repository,
        ProjectEnvironmentRepository $repositoryEnv,
        FormFactoryInterface $factory,
        RouterInterface $router,
        FlashMessageService $flashMessageService,
        ShellService $shellService
    ) {
        $id = $request->get('id');

        $entity = new ProjectEntity();

        if ($id) {
            $findEntity = $repository->findOneById($id);
            if ($findEntity) {
                $entity = $findEntity;
            } else {
                throw new NotFoundHttpException();
            }
        }

        $form = $factory->create(ProjectFormType::class, $entity);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            if ($entity->getId()) {

                $repository->update($entity);
                $flashMessageService->createSuccessMessage('project.was.updated');

            } else {

                $repository->insert($entity);

                $productionEnv = (new ProjectEnvironmentEntity())
                    ->setType(ProjectEnvironmentTypeEnumType::TYPE_PRODUCTION)
                    ->setProject($entity);

                $stagingEnv = (new ProjectEnvironmentEntity())
                    ->setType(ProjectEnvironmentTypeEnumType::TYPE_STAGING)
                    ->setProject($entity);

                $entity
                    ->addProjectEnvironments($productionEnv)
                    ->addProjectEnvironments($stagingEnv);

                $repositoryEnv->insert($productionEnv);
                $repositoryEnv->insert($stagingEnv);

                $repository->update($entity);

                $projectIdentificator = $entity->getIdentificator();
                $serverAlis = getenv("SERVER_ALIAS");

                $shellService->executeBulk(
                    [
//                        "adduser $projectIdentificator",
                        "mkdir /home/$projectIdentificator/.ssh -p",
                        "ssh-keygen -b 2048 -t ed25519 -f /home/$projectIdentificator/.ssh/id_rsa -q -N '' -C '$serverAlis - $projectIdentificator' ",
                    ]
                );

                $flashMessageService->createSuccessMessage('project.was.created');
            }

            $url = $router->generate('cms_project_default');

            return new RedirectResponse($url);
        }

        $public = null;
        if($entity->getId()) {
            $projectIdentificator = $entity->getIdentificator();
            $result = $shellService->execute("cat /home/$projectIdentificator/.ssh/id_rsa.pub");
            $public = $result['output'][0];
        }

        return [
            'heading' => $entity->getId() ? 'edit.project' : 'add.new.project',
            'form' => $form->createView(),
            'notes' => ['public_key' => $public],
        ];
    }
}