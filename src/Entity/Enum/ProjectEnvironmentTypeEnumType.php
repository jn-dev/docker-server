<?php

namespace App\Entity\Enum;

class ProjectEnvironmentTypeEnumType extends AbstractEnumType
{
    CONST TYPE_PRODUCTION = 'production';
    CONST TYPE_STAGING = 'staging';
    CONST TYPE_TESTING = 'testing';

    public $name = 'projectEnvironmentTypeEnumType';

    public $values = [
        self::TYPE_PRODUCTION,
        self::TYPE_STAGING,
        self::TYPE_TESTING,
    ];
}