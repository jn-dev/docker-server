<?php

namespace App\Entity;

use App\Entity\Enum\ProjectEnvironmentTypeEnumType;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ProjectEnvironmentRepository")
 * @ORM\Table(name="project_environment")
 */
class ProjectEnvironmentEntity extends AbstractEntity
{
    /**
     * @var null|int
     *
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(type="projectEnvironmentTypeEnumType")
     */
    private $type;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     */
    private $name = '';

    /**
     * @var null|ProjectEntity
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\ProjectEntity", inversedBy="projectEnvironments")
     * @ORM\JoinColumn(name="project_id", referencedColumnName="id")
     */
    private $project;

    /**
     * @var string
     *
     * @ORM\Column(type="text")
     */
    private $vars = '';

    /**
     * @var string
     *
     * @ORM\Column(type="text", nullable=true)
     */
    private $deploymentScenario = '';

    /**
     * @return int|null
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int|null $id
     *
     * @return self
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return null|string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param null|string $type
     *
     * @return self
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @return null|string
     */
    public function getName()
    {
        if(in_array($this->getType(), [ProjectEnvironmentTypeEnumType::TYPE_PRODUCTION, ProjectEnvironmentTypeEnumType::TYPE_STAGING])) {
            return $this->getType();
        }

        return $this->name;
    }

    /**
     * @param null|string $name
     *
     * @return self
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return ProjectEntity|null
     */
    public function getProject()
    {
        return $this->project;
    }

    /**
     * @param ProjectEntity|null $project
     *
     * @return self
     */
    public function setProject($project)
    {
        $this->project = $project;

        return $this;
    }

    /**
     * @return null|string
     */
    public function getVars()
    {
        return $this->vars;
    }

    /**
     * @param null|string $vars
     *
     * @return self
     */
    public function setVars($vars)
    {
        $this->vars = $vars;

        return $this;
    }

    /**
     * @return null|string
     */
    public function getDeploymentScenario()
    {
        return $this->deploymentScenario;
    }

    /**
     * @param null|string $deploymentScenarion
     *
     * @return self
     */
    public function setDeploymentScenario($deploymentScenario)
    {
        $this->deploymentScenario = $deploymentScenario;

        return $this;
    }

}
