<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ProjectRepository")
 * @ORM\Table(name="project")
 */
class ProjectEntity extends AbstractEntity
{
    /**
     * @var null|int
     *
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var null|string
     *
     * @ORM\Column(type="string")
     */
    private $name;

    /**
     * @var null|string
     *
     * @ORM\Column(type="string", unique=true)
     *
     * @Assert\Regex(
     *     match=true,
     *     pattern="/^[a-z]+$/",
     *     message="Name must be unique and [a-z]+"
     * )
     */
    private $identificator;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="App\Entity\ProjectEnvironmentEntity", mappedBy="project")
     */
    private $projectEnvironments;

    /**
     * @var null|string
     *
     * @ORM\Column(type="text")
     */
    private $dockerComposeTemplate;

    /**
     * @var null|string
     *
     * @ORM\Column(type="text", nullable=true)
     */
    private $vars;

    /**
     * ProjectEntity constructor.
     */
    public function __construct()
    {
        $this->projectEnvironments = new ArrayCollection();
    }

    /**
     * @return int|null
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int|null $id
     *
     * @return self
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return null|string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param null|string $name
     *
     * @return self
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return null|string
     */
    public function getDockerComposeTemplate()
    {
        return $this->dockerComposeTemplate;
    }

    /**
     * @param null|string $dockerComposeTemplate
     *
     * @return self
     */
    public function setDockerComposeTemplate($dockerComposeTemplate)
    {
        $this->dockerComposeTemplate = $dockerComposeTemplate;

        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getProjectEnvironments()
    {
        return $this->projectEnvironments;
    }

    /**
     * @param ArrayCollection $projectEnvironments
     *
     * @return self
     */
    public function setProjectEnvironments($projectEnvironments)
    {
        $this->projectEnvironments = $projectEnvironments;

        return $this;
    }

    /**
     * @param ProjectEnvironmentEntity $projectEnvironment
     *
     * @return $this
     */
    public function addProjectEnvironments(ProjectEnvironmentEntity $projectEnvironment)
    {
        $this->projectEnvironments->add($projectEnvironment);

        return $this;
    }

    /**
     * @return null|string
     */
    public function getVars()
    {
        return $this->vars;
    }

    /**
     * @param null|string $vars
     *
     * @return self
     */
    public function setVars($vars)
    {
        $this->vars = $vars;

        return $this;
    }

    /**
     * @return null|string
     */
    public function getIdentificator()
    {
        return $this->identificator;
    }

    /**
     * @param null|string $identificator
     *
     * @return self
     */
    public function setIdentificator($identificator)
    {
        $this->identificator = $identificator;

        return $this;
    }

    public function __toString()
    {
        return $this->getName();
    }
}
