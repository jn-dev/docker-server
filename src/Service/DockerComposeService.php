<?php

namespace App\Service;

use App\Entity\ProjectEnvironmentEntity;

/**
 * Class DockerComposeService
 * @package App\Service
 */
class DockerComposeService
{
    /**
     * @var ContextService
     */
    private $contextService;

    /**
     * @var ShellService
     */
    private $shellService;

    /**
     * @var ProjectService
     */
    private $projectService;

    /**
     * @var string
     */
    private $dockerComposeFilePath;

    /**
     * DockerComposeService constructor.
     *
     * @param ContextService $contextService
     * @param ShellService   $shellService
     * @param ProjectService $projectService
     */
    public function __construct(
        ContextService $contextService,
        ShellService $shellService,
        ProjectService $projectService
    ) {
        $this->contextService = $contextService;
        $this->shellService = $shellService;
        $this->projectService = $projectService;
    }


    public function deploy(ProjectEnvironmentEntity $env)
    {

        $commands = explode('\n', $env->getDeploymentScenario());


        return $this->executeCommand($env, $commands);
    }


    private function getWorkDir()
    {
        $workSpace = $this->contextService->getCacheDir().'/app/docker-compose';
        if (!file_exists($workSpace)) {
            mkdir($workSpace, 0770, true);
        }

        return $workSpace;
    }

    private function getDockerComposeFilePath() {

        if(!$this->dockerComposeFilePath) {
            $this->dockerComposeFilePath = $this->getWorkDir()."/".time().".docker-compose.yml";
        }

        return $this->dockerComposeFilePath;
    }

    /**
     * @param ProjectEnvironmentEntity $env
     * @param array                    $commands
     *
     * @return array with exec result
     */
    private function executeCommand(ProjectEnvironmentEntity $env, array $commands)
    {
        $dockerComposeFile = $this->getDockerComposeFilePath();
        $identificator = $env->getProject()->getIdentificator();

        file_put_contents($dockerComposeFile, $this->projectService->createDockerCompose($env));

        $dockerComposeCommands = [];
        foreach ($commands as $cmd) {
            $dockerComposeCommands[] = "docker-compose -f $dockerComposeFile -p $identificator $cmd";
        }

        $results = $this->shellService->executeBulk($dockerComposeCommands);

        //clean up and return result
        unlink($dockerComposeFile);

        return $results;
    }

}
