<?php

namespace App\Service;

use App\Entity\UserEntity;
use App\Kernel;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

/**
 * Class ContextService
 *
 * @package App\Service
 */
class ContextService
{
    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * @var TokenStorageInterface
     */
    private $tokenStorage;

    /**
     * ContextService constructor.
     *
     * @param ContainerInterface    $container
     * @param TokenStorageInterface $tokenStorage
     */
    public function __construct(ContainerInterface $container, TokenStorageInterface $tokenStorage)
    {
        $this->container = $container;
        $this->tokenStorage = $tokenStorage;
    }


    /**
     * Return environment set from bootstrep
     *
     * @return string (dev|prod|test|...)
     */
    public function getEnvironment()
    {
        /** @var Kernel $kernel */
        $kernel = $this->container->get('kernel');

        return $kernel->getEnvironment();
    }

    /**
     * Return environment set from bootstrep
     *
     * @return string cache directory
     */
    public function getCacheDir()
    {
        /** @var Kernel $kernel */
        $kernel = $this->container->get('kernel');

        return $kernel->getCacheDir();
    }

    /**
     * Return parameter from config
     *
     * @param string $parameterName
     *
     * @return mixed
     * @throws \Exception
     */
    public function getParameter(string $parameterName)
    {
        if ($this->container->hasParameter($parameterName)) {

            return $this->container->getParameter($parameterName);
        }

        throw new \Exception("Parameter '$parameterName' not found.");
    }

}
