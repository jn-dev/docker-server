<?php

namespace App\Service;

use App\Entity\Enum\ProjectEnvironmentTypeEnumType;
use App\Entity\ProjectEnvironmentEntity;
use Symfony\Component\Yaml\Yaml;

/**
 * Class ProjectService
 * @package App\Service
 */
class ProjectService
{
    public function createDockerCompose(ProjectEnvironmentEntity $projectEnvironmentEntity)
    {
        $baseProjectName = preg_replace("/[^a-zA-Z0-9]+/", "", $projectEnvironmentEntity->getProject()->getName());

        $loader = new \Twig_Loader_Array(
            ['tpl' => $projectEnvironmentEntity->getProject()->getDockerComposeTemplate()]
        );
        $twig = new \Twig_Environment($loader);


        $projectVars = Yaml::parse($projectEnvironmentEntity->getProject()->getVars());
        $envVars = Yaml::parse($projectEnvironmentEntity->getVars());

        //pokud nic nevyplnim
        $projectVars = is_array($projectVars) ? $projectVars : [];
        $envVars = is_array($envVars) ? $envVars : [];

        $variables = array_merge($projectVars, $envVars);
        $variables['environment'] = $projectEnvironmentEntity->getType();

        $twigOutput = $twig->render('tpl', $variables);

        $yamlTemplate = Yaml::parse($twigOutput);

        //creat container names
        if (isset($yamlTemplate['services'])) {
            $buildTime = (new \DateTime())->format('Y.m.d_H.i.s');

            foreach ($yamlTemplate['services'] as $serviceKey => $service) {

                $containerName = 'ds_'.$baseProjectName.'.'.$serviceKey.'_'.$projectEnvironmentEntity->getType();

                if ($projectEnvironmentEntity->getType() == ProjectEnvironmentTypeEnumType::TYPE_PRODUCTION) {
                    $containerName .= '_'.$buildTime;
                }

                $yamlTemplate['services'][$serviceKey]['container_name'] = $containerName;

                //Status dovolime jen pri staging/production ostatni smazeme rovnou volumes
                if (isset($service['volumes'])) {

                    if (
                        $projectEnvironmentEntity->getType() === ProjectEnvironmentTypeEnumType::TYPE_PRODUCTION ||
                        $projectEnvironmentEntity->getType() === ProjectEnvironmentTypeEnumType::TYPE_STAGING
                    ) {

                        foreach ($service['volumes'] as $volumeKey => $volumeValue) {

                            $volume = explode(':', $volumeValue);
                            $dockerServerDestination = $volume[0];
                            $containerDestination = $volume[1];

                            $volumeDir = getenv('VOLUME_DIRECTORY');

                            $fullDestinationArray = [
                                $volumeDir,
                                $projectEnvironmentEntity->getProject()->getIdentificator(),
                                $serviceKey,
                                $dockerServerDestination,
                            ];
                            $yamlTemplate['services'][$serviceKey]['volumes'][$volumeKey] = implode('/', $fullDestinationArray).":".$containerDestination;

                        }

                    } else {
                        unset($yamlTemplate['services'][$serviceKey]['volumes']);
                    }
                }
            }
        }

        return Yaml::dump($yamlTemplate, 100);
    }
}
