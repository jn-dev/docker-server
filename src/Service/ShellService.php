<?php

namespace App\Service;

/**
 * Class ShellService
 * @package App\Service
 */
class ShellService
{

    public function execute(string $cmd)
    {

        $output = null;
        $exitCode = null;

        exec($cmd." 2>&1", $o, $exitCode);

        $output['cmd'] = $cmd;
        $output['output'] = $o;
        $output['exit'] = $exitCode;

        return $output;
    }

    public function executeBulk(array $commands)
    {

        $cmdBulk = [];

        //run commands
        foreach ($commands as $cmd) {

            $result = $this->execute($cmd);
            $cmdBulk[] = $result;

            if ($result['exit'] > 0) {
                break;
            }
        }

        return $cmdBulk;
    }
}
