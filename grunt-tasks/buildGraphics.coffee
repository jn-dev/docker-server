'use strict'

module.exports = (grunt) ->
  grunt.registerMultiTask 'buildGraphics', ':-O', ->
    options = @options(
      imagesPath: undefined
      objectName: undefined
      outputScriptFile: undefined
    )

    fileExists = require('file-exists')
    directoryExists = require('directory-exists');
    del = require('del')
    find = require('find');
    writeFile = require('write')
    readFile = require('read-file')


    # Check
    if options.imagesPath == ''
      grunt.log.warn 'Option `imagesPath` is required.'
      return false

    if options.objectName == ''
      grunt.log.warn 'Option `objectName` is required.'
      return false

    if options.basePath == ''
      grunt.log.warn 'Option `outputScriptFile` is required.'
      return false

    if not directoryExists.sync(options.imagesPath)
      grunt.log.warn "imagesPath '" + options.imagesPath + "' not exists."

    #parse icons
    icons = {}
    files = find.fileSync(/\.svg$/, options.imagesPath)
    for filePath in files
      name = filePath.replace(/^.*[\\\/]/, '').replace(".svg","");
      buffer = readFile.sync(filePath)
      content = buffer.toString('utf8').replace(/\s/g, " ").replace('<?xml version="1.0" encoding="utf-8"?>', '').trim()
      icons[name] = content;

    #create file
    outputString = "var " + options.objectName + ' = {'
    for icon, content of icons

      outputString += '\n    \''+icon+'\': \'' + content + '\','

    #odstrait posledni carku
    outputString.slice(-1);

    outputString += '\n}'

    # Uložení výsledného JS
    writeFile.sync(options.outputScriptFile, outputString)

    grunt.log.ok "graphics.js created."

    return

  return